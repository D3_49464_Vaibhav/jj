import javax.xml.stream.events.Namespace;

class Hello{
    public static void main( String[] args ){
        //printf("Hello World\n");    //C
        
        /* #include<iostream>
        using namespace std;
        cout << "Hello World" <<endl; */

        System.out.println("Hello World\n");
        //System : It is a final class declared in java.lang package
        //out : It is reference variable of java.io.PrintStream class. It is public static final field of System class.
        //println : It is a non static method of java.io.PrintStream class.
    }
}