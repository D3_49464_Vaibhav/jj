package org.sunbeam.dac.test;
public class Calculator {
	public static int sum( int num1, int num2 ) {
		return num1 + num2;
	}
	public static int sub( int num1, int num2 ) {
		return num1 - num2;
	}
	public static int multiplication( int num1, int num2 ) {
		return num1 * num2;
	}
	public static int division( int num1, int num2 ) {
		return num1 / num2;
	}
}
